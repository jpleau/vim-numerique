function! numerique#strip(input_string)
    return substitute(a:input_string, '^\s*\(.\{-}\)\s*$', '\1', '')
endfunction

function! numerique#module_path()
	let l:file = split(expand("%"), "/")
	let l:module_path = get(l:file, 0) . "/" . get(l:file, 1)
	return l:module_path
endfunction

function! numerique#open_file(file)
	execute "edit " . a:file
endfunction

function! numerique#switch(newfile)
	let l:filename = expand("%")
	let l:newfile = numerique#module_path() . "/" . a:newfile

	if l:filename !=# l:newfile
		call numerique#open_file(l:newfile)
	endif
endfunction

function! numerique#switch_tpl(...)
	let l:filename = expand("%:t")

	let l:check_action = 0
	if a:0 > 0
		let l:check_action = a:1
	endif

	if l:check_action == 1
		if l:filename !=# "module.php"
			return
		endif

		let l:aliases = {
			\ "intranet-ajouter": "intranet-formulaire",
			\ "intranet-editer": "intranet-formulaire",
			\ "intranet-supprimer": ""
		\ }
		let l:pattern = 'case "\(site\|intranet\)-\(.*\)"'
		let l:linenumber = search(l:pattern, 'bn')
		let l:line = numerique#strip(getline(l:linenumber))
		let l:match = matchlist(l:line, l:pattern)
		if len(l:match) > 0
			let l:template = join(l:match[1:2], "-")
			if has_key(l:aliases, l:template)
				let l:alias = l:aliases[l:template]
				if len(l:alias) > 0
					let l:template = l:alias
				else
					return
				endif
			endif
			if l:template !~ "traitement"
				let l:file = numerique#module_path() . "/tpl/" . l:template . ".tpl"
				let l:answers = { 'y': 1, 'yes': 1 }
				let l:input = printf('%s: file does not exist. Create it? y\n ', l:template . ".tpl")
				if filereadable(l:file) || has_key(l:answers, tolower(input(l:input)))
					call numerique#open_file(l:file)
				endif
			endif
		endif
	else
		let l:tpl_path = numerique#module_path() . "/tpl/"
		let l:cmd = printf("edit %s<tab>", l:tpl_path)
		call feedkeys(":edit " . l:tpl_path . "\<TAB>\<TAB>", "t")
	endif
endfunction!
