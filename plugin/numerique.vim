"if exists("g:loaded_numerique") || &cp || v:version < 700
"  finish
"endif
"let g:loaded_numerique = 1

nnoremap <silent> <Plug>NumeriqueModule :call numerique#switch("module.php")<cr>
nnoremap <silent> <Plug>NumeriqueClasse :call numerique#switch("classe.php")<cr>
nnoremap <silent> <Plug>NumeriqueLangues :call numerique#switch("langues.php")<cr>
nnoremap <silent> <Plug>NumeriqueTpl :call numerique#switch_tpl()<cr>
nnoremap <silent> <Plug>NumeriqueActionTpl :call numerique#switch_tpl(1)<cr>

com! Num :source ~/test/vim/numerique/autoload/numerique.vim

augroup numerique_grp
	autocmd!
	autocmd BufWinEnter,BufWritePost */.git/ftpdata :set filetype=ini
augroup END
